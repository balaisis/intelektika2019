﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Threading;

namespace IntelektikosGrupinisDarbas
{

    class FootballMatchData
    {
        DateTime Date { get; set; }
        string HomeTeam { get; set; }
        string AwayTeam { get; set; }
        int HomeScore { get; set; }
        int AwayScore { get; set; }
        string Tournament { get; set; }
        int PlayedAtHome { get; set; }
        double Distance { get; set; }

        public FootballMatchData()
        {

        }

        public FootballMatchData(string date, string homeTeam, string awayTeam, int homeScore, int awayScore, string tournament,
            int playedAtHome)
        {
            Date = DateTime.Parse(date);
            HomeTeam = homeTeam;
            AwayTeam = awayTeam;
            HomeScore = homeScore;
            AwayScore = awayScore;
            Tournament = tournament;
            PlayedAtHome = playedAtHome;
            Distance = 0.0;
        }

        public string GetDate()
        {
            return Date.Date.ToString("yyyy-MM-dd");
        }

        public void SetDate(string DateString)
        {
            Date = DateTime.Parse(DateString);
        }

        public string GetHomeTeam()
        {
            return HomeTeam;
        }

        public void SetHomeTeam(string HomeTeamString)
        {
            HomeTeam = HomeTeamString;
        }

        public string GetAwayTeam()
        {
            return AwayTeam;
        }

        public void SetAwayTeam(string AwayTeamString)
        {
            AwayTeam = AwayTeamString;
        }

        public int GetHomeScore()
        {
            return HomeScore;
        }

        public void SetHomeScore(int HomeScoreInt)
        {
            HomeScore = HomeScoreInt;
        }

        public int GetAwayScore()
        {
            return AwayScore;
        }

        public void SetAwayScore(int AwayScoreInt)
        {
            AwayScore = AwayScoreInt;
        }

        public string GetTournament()
        {
            return Tournament;
        }

        public void SetTournament(string TournamentString)
        {
            Tournament = TournamentString;
        }


        public int GetPlayedAtHome()
        {
            return PlayedAtHome;
        }

        public void SetPlayedAtHome(int PlayedAtHomeInt)
        {
            PlayedAtHome = PlayedAtHomeInt;
        }

        public double GetDistance()
        {
            return Distance;
        }

        public void SetDistance(double DistanceDouble)
        {
            Distance = DistanceDouble;
        }

        public override string ToString()
        {
            return string.Format("Match date: {0} \nHome team name: {1} | Away team name: {2} \nHome team Score: {3} | Away team Score: {4} \nTournament name: {5} \nMatch played at home: {6}",
                GetDate(), GetHomeTeam(), GetAwayTeam(), GetHomeScore(), GetAwayScore(), GetTournament(), GetPlayedAtHome());
        }
    }

    class Program
    {
        const string csvFileLocation = "..\\..\\csvFiles\\results.csv";

        static void Main(string[] args)
        {
           bool endProgram = false;
            while (!endProgram)
            {
                endProgram = true;
                RunProgram(ref(endProgram));
            }
        }

        static void RunProgram(ref bool endProgram)
        {
            List<double> accuracyList = new List<double>();
            Dictionary<int, FootballMatchData> hashtable = ReadCsvData(csvFileLocation);
            Console.WriteLine("Home: {0} Away: {1}", hashtable.Values.Where(x => x.GetPlayedAtHome() == 1).ToList().Count, hashtable.Values.Where(x => x.GetPlayedAtHome() == 0).ToList().Count);
            int k = 0;
            bool validInput = false;
            while(!validInput || k < 1)
            {
                Console.Write("Please enter the K value: ");
                Console.ForegroundColor = ConsoleColor.Green;
                string input = Console.ReadLine();
                Console.ResetColor();
                validInput = int.TryParse(Convert.ToString(input), out k);
                if (!validInput || k < 1)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid entry. Please enter positive integer value.");
                    Console.ResetColor();
                }
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Entry is valid. Calculating...");
            Console.ResetColor();
            double accuracySum = 0;
            Thread[] taskArray = new Thread[10];
            for (int i = 1; i <= 10; i++)
            {

                int localnum = i;
                taskArray[i - 1] = new Thread(() => Execute( ref (k), ref (hashtable), ref (localnum), ref (accuracySum)));
            }
            var watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 1; i <= 10; i++)
                taskArray[i - 1].Start();
            for (int i = 1; i <= 10; i++)
                taskArray[i - 1].Join();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Average Accuracy: {0:0.000}%", accuracySum / 10 * 100);
            watch.Stop();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Time elapsed: {0} seconds", watch.ElapsedMilliseconds * 0.001);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Do you want to run this program again?");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Press Y to run this program again.");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Press any other key to exit the program.");
            Console.ResetColor();
            var decision = Console.ReadKey();
            if(decision.Key == ConsoleKey.Y)
            {
                endProgram = false;
            }
            else
            {
                endProgram = true;
            }
            Console.WriteLine();
        }

        static void Execute(ref int k, ref Dictionary<int, FootballMatchData> hashtable, ref int section, ref double accuracySum)
        {

            int truePositive = 0;
            int falsePositive = 0;
            int trueNegative = 0;
            int falseNegative = 0;
            KNeighboursAlgorithm(ref (hashtable), section, ref (truePositive), ref (falsePositive), ref (trueNegative), ref (falseNegative), ref (k));


            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Segment {0,2}: ", section);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("TP: ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("{0,10}", truePositive);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" FP: ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("{0,10}", falsePositive);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" TN: ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("{0,10}", trueNegative);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" FN: ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("{0,10}", falseNegative);
            double correctEntries = truePositive + trueNegative;
            double falseEntries = falsePositive + falseNegative;
            double accuracy = correctEntries / (correctEntries + falseEntries);
            accuracySum = accuracySum + accuracy;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("   Accuracy: {0:0.000}%", accuracy*100);
            Console.ResetColor();
        }

        static Dictionary<int, FootballMatchData> ReadCsvData(string csvFileLocation)
        {
            Dictionary<int, FootballMatchData> hashtable = new Dictionary<int, FootballMatchData>();
            using (StreamReader reader = new StreamReader(csvFileLocation))
            {                
                string text = reader.ReadToEnd();
                string[] lines = text.Split('\n');
                int counter = lines.Length;
                bool firstLine = true;
                foreach(string line in lines)
                {
                    if(firstLine)
                    {
                        firstLine = false;
                        continue;
                    }
                    string[] data = line.Split(',');
                    string date = data[0];
                    string homeTeam = data[1];
                    string awayTeam = data[2];
                    int homeScore = int.Parse(data[3]);
                    int awayScore = int.Parse(data[4]);
                    string tournament = data[5];
                    int playedAtHome = int.Parse(data[6]);
                    FootballMatchData entry = new FootballMatchData(date, homeTeam, awayTeam, homeScore, awayScore, tournament, playedAtHome);
                    hashtable.Add((lines.Length - counter), entry);
                    counter--;
                }
                reader.Close();
            }
            return hashtable;
        }

        static void KNeighboursAlgorithm(ref Dictionary<int, FootballMatchData> hashtable , int section, ref int truePositive, ref int falsePositive,
            ref int trueNegative, ref int falseNegative, ref int k)
        {


            List<FootballMatchData> TrainingData = new List<FootballMatchData>();
            List<FootballMatchData> TestData = new List<FootballMatchData>();
            int lowestTestDataPointer = 0;
            int highestTestDataPointer = 0;
            if (section - 1 == 0)
                lowestTestDataPointer = 0;
            else
                lowestTestDataPointer = hashtable.Count / 10 * (section - 1);
             highestTestDataPointer = hashtable.Count / 10 * section;
            for (int i = lowestTestDataPointer; i < highestTestDataPointer; i++)
            {
                TestData.Add(hashtable[i]);
            }

            for(int i = 0; i < hashtable.Count; i++)
            {
                if(i < lowestTestDataPointer || i >= highestTestDataPointer)
                {
                    TrainingData.Add(hashtable[i]);
                }
            }

            foreach (FootballMatchData item in TestData)
            {
              EuclideanDistance(ref(TrainingData), item, ref (truePositive), ref (falsePositive), ref (trueNegative), ref (falseNegative), ref(k));
            }

               
        }

        static void EuclideanDistance(ref List<FootballMatchData> TrainingDataList, FootballMatchData TestItem, ref int truePositive, ref int falsePositive,
            ref int trueNegative, ref int falseNegative, ref int k)
        {
            double d = 0.0;

            foreach (FootballMatchData item in TrainingDataList)
            {
                    double temp = (item.GetHomeScore() - TestItem.GetHomeScore()) *
                        (item.GetHomeScore() - TestItem.GetHomeScore());
                    double temp2 = (item.GetAwayScore() - TestItem.GetAwayScore()) *
                        (item.GetAwayScore() - TestItem.GetAwayScore());
                    double sum = temp + temp2;
                    d = Math.Sqrt(sum);
                item.SetDistance(d);
            }
            Object thisLock = new Object();
            lock(thisLock)
                FindKNeighbours(ref (TrainingDataList), ref(TestItem), ref (truePositive), ref (falsePositive), ref (trueNegative), ref (falseNegative), ref(k));
        }

        static void FindKNeighbours(ref List<FootballMatchData> TrainingData, ref FootballMatchData TestItem, ref int truePositive, ref int falsePositive,
    ref int trueNegative, ref int falseNegative, ref int k)
        {
            List<FootballMatchData> sortedList = new List<FootballMatchData>(TrainingData);
            sortedList.OrderBy(x => x.GetDistance());
            int neighbourCounter = 0;
            int homeCounter = 0;
            int awayCounter = 0;


            while(neighbourCounter != k)
            {
                foreach (FootballMatchData entry in sortedList)
                {
                    if (entry.GetAwayScore() > entry.GetHomeScore())
                        awayCounter++;
                    if (entry.GetAwayScore() < entry.GetHomeScore())
                        homeCounter++;
                    neighbourCounter++;
                    if (neighbourCounter == k)
                        break;
                }
            }

            if(awayCounter > homeCounter && TestItem.GetPlayedAtHome() == 1)
            {
                trueNegative++;
            }
            else if(awayCounter < homeCounter && TestItem.GetPlayedAtHome() == 1)
            {
                truePositive++;
            }
            else if(awayCounter > homeCounter && TestItem.GetPlayedAtHome() == 0)
            {
                falseNegative++;
            }
            else if (awayCounter < homeCounter && TestItem.GetPlayedAtHome() == 0)
            {
                falsePositive++;
            }
        }
    }
}
